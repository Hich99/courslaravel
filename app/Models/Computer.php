<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use \App\Models\Presenters\ComputerPresenter;
use Illuminate\Database\Eloquent\SoftDeletes;

class Computer extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $fillable = [
        'name' ,
        'slug' ,
        'brand_id' ,
        'comment' ,
        'is_available' ,
        'image' ,
    ];
}
